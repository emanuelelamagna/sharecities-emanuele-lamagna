import 'dart:convert';

import 'package:applicazione_tesi/genericComponents/buttons/mainButton.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/normalText.dart';
import 'package:applicazione_tesi/genericComponents/titleText.dart';
import 'package:applicazione_tesi/listRoomsPage/listRoomsPage.dart';
import 'package:applicazione_tesi/models/city.dart';
import 'package:applicazione_tesi/models/freePlace.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/roomPage/roomPage.dart';
import 'package:applicazione_tesi/services/favCitiesService.dart';
import 'package:applicazione_tesi/services/getCitiesService.dart';
import 'package:applicazione_tesi/services/roomsService.dart';
import 'package:applicazione_tesi/services/locationService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import '../genericComponents/logoAndBackButton.dart';

class _CityPage extends State<CityPage> {

  final ScrollController _scrollController = new ScrollController();
  final City _city;
  List<City> _favCities = [];
  List<FreePlace> _attractionsPosition = [];
  List<Room> _closeRooms = [];
  String _imagePath = 'assets/images/cityDay.jpg';
  int _itemSelected;
  bool _isDay = true;
  bool _favorite;
  double _infoWidth = 0;
  double _infoHeight = 0;
  String _textInfo = "";
  Offset _tapPosition;

  _CityPage(this._city, this._favorite, this._favCities) {
    this._getFreePlaces(int.parse(this._city.cityID));
    if (this._city.name == 'Cesena') {
      _imagePath = 'assets/images/cesena.jpg';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
            alignment: AlignmentDirectional.topCenter,
            children: <Widget>[
              Container(
              decoration: Decorations.imageLogin()),
              GestureDetector(
                  onTapDown: _handleTapDown ,
                  child: Center(child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 30,),
                      LogoAndBackButton(),
                      SizedBox(height: 20,),
                      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                        TitleText(this._city.name, TextAlign.left, false),
                        AnimatedContainer(
                            duration: Duration(milliseconds: 700),
                            height: 30,
                            width: 80,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: _isDay ? Colors.yellow[700] : Colors.blue[700]
                            ),
                            child: Stack(
                              children: <Widget>[
                                AnimatedPositioned(
                                    duration: Duration(milliseconds: 700),
                                    curve: Curves.easeIn,
                                    top: 3,
                                    left: this._isDay ? 0 : 50,
                                    right: this._isDay ? 50 : 0,
                                    child: InkWell(
                                        onTap: toggleButton,
                                        child: AnimatedSwitcher(
                                            duration: Duration(milliseconds: 700),
                                            child: _isDay ? Icon(Icons.wb_sunny, color: Colors.yellow[400], size: 25, key: UniqueKey()) :
                                            Icon(Icons.star_border, color: Colors.blue[400], size: 25, key: UniqueKey())
                                        )
                                    )
                                )
                              ],
                            )
                        ),
                        IconButton(
                          icon: Icon(this._favorite ? Icons.star : Icons.star_border,
                            size: 35, color: Colors.orange[200],),
                          onPressed: this._insertNewFav,
                        ),
                      ],),
                      Container(
                        height: 400,
                        child:Stack(children: <Widget>[
                          SafeArea(
                            child: NotificationListener(
                                child: SingleChildScrollView(
                                    controller: this._scrollController,
                                    scrollDirection: Axis.horizontal,
                                    child: Center(
                                        child: Row(
                                          children: <Widget>[
                                            Image.asset(this._imagePath),
                                          ],
                                        )
                                    )
                                )
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 30,
                              width: 30,
                              child: Tab(
                              icon: Image.asset('assets/images/swipe.jpg')),
                            ),
                          ),

                          Padding(
                              padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
                              child:
                              Align(
                                  alignment: Alignment.topCenter,
                                  child:
                                  AnimatedContainer(
                                    curve: Curves.easeInOutQuart,
                                    duration: Duration(milliseconds: 500),
                                    height: this._infoHeight,
                                    width: 300,
                                    decoration: Decorations.itemDecorationContainer(),
                                    child: Center(child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                                     NormalText("Room of " + this._textInfo, false),
                                      MainButton("See", () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => RoomPage(this._closeRooms[this._itemSelected], this._closeRooms)
                                            )
                                        );
                                      }, 75),
                                    ],)),
                                  )))
                        ],),
                      ),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child:
                          Padding(
                              padding: EdgeInsets.all(20),
                              child:
                              Container(
                                  decoration: Decorations.loginButtons(),
                                  child: SecondaryButton("All the rooms", () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => ListRoomsPage(this._city)));
                                  }))
                          )),
                    ],),
                  ))
        ]),
    );
  }

  void _getFreePlaces(final int cityID) async {
    final FlutterSecureStorage storage = FlutterSecureStorage();
    //String _result = await storage.read(key: 'freePlaces'+cityID.toString());
    String _result = null;
    List<FreePlace> freePlaces = [];
    if (_result == null){
      freePlaces = await GetCitiesService().attemptGetFreePlaces(cityID);
    } else {
      for(var i = 0; i < json.decode(_result).length; i++) {
        freePlaces.add(FreePlace.fromJson(json.decode(_result)[i]));
      }
    }
    //String _resultCloseRooms = await storage.read(key: 'closeRooms-'+this._city.name);
    String _resultCloseRooms = null;
    List<Room> closeRooms = [];
    if (_resultCloseRooms == null){
      final position = await LocationService().getCurrentPosition();
      closeRooms = await RoomsService().attemptGetCloseRooms(this._city.name, this._city.country, position, 5000);
    } else {
      for(var i = 0; i < json.decode(_resultCloseRooms).length; i++) {
        closeRooms.add(Room.fromJson(json.decode(_resultCloseRooms)[i]));
      }
    }
    if (freePlaces != null) {
      print(freePlaces.length);
      this._attractionsPosition.addAll(freePlaces);
    }
    if (closeRooms != null) {
      this._closeRooms.addAll(closeRooms);
    }
  }

  void _insertNewFav() async {
    FavCitiesService().attemptNewFavCity(this._city.name, this._city.country).then((result) {
      if(!result) {
        CustomAlertDialog(context, "Error", "There was an error adding the preferred city.").show();
      } else{
        this.setState(() {
          this._favorite = !this._favorite;
          if (this._favorite) {
            this._favCities.add(_city);
          } else {
            var index = 0;
            for (int i = 0; i < this._favCities.length; i++) {
              if (_city.name == this._favCities[i].name && _city.country == this._favCities[i].country) {
                index = i;
              }
            }
            this._favCities.removeAt(index);
          }
        });
      }
    });
  }

  void toggleButton() {
    this.setState(() {
      _isDay = !_isDay;
      if (this._isDay) {
        if (this._city.name == 'Cesena') {
          _imagePath = 'assets/images/cesena.jpg';
        } else {
          _imagePath = 'assets/images/cityDay.jpg';
        }
      } else {
        if (this._city.name == 'Cesena') {
          _imagePath = 'assets/images/cesenaNight.jpg';
        } else {
          _imagePath = 'assets/images/cityNight.jpg';
        }
      }
    });
  }

  void _handleTapDown(TapDownDetails details) {
    final RenderBox referenceBox = context.findRenderObject();
    final scrollX = this._scrollController.position.pixels;

    this._tapPosition = referenceBox.globalToLocal(details.globalPosition);
    print("\nX: " + _tapPosition.dx.toString());
    print("Y: " + _tapPosition.dy.toString());
    print("Offset: " + scrollX.toString());

    for (int i = 0; i < this._attractionsPosition.length; i++) {
      final offsetX = this._attractionsPosition[i].offset;
      if (_tapPosition.dx >= this._attractionsPosition[i].startX - scrollX + offsetX
          && _tapPosition.dx <= this._attractionsPosition[i].endX - scrollX + offsetX
          && _tapPosition.dy >= this._attractionsPosition[i].startY
          && _tapPosition.dy <= this._attractionsPosition[i].endY) {

        if (this._itemSelected != i) {
          this._itemSelected = i;
          this.setState(() {
            this._infoHeight = 50;
            this._infoWidth = 300;
          });
        } else {
          this._itemSelected = -1;
          this.setState(() {
            this._infoHeight = 0;
            this._infoWidth = 0;
          });
        }

        this._changeTextInfo(i);
      }
    }
  }

  void _changeTextInfo(final int roomIndex) {
    this.setState(() {
      if (roomIndex < this._closeRooms.length) {
        this._textInfo = this._closeRooms[roomIndex].name;
      }
    });
  }

}

class CityPage extends StatefulWidget {

  final City _city;
  final bool _favorite;
  final List<City> _favCities;

  CityPage(this._city, this._favorite, this._favCities);

  @override
  State<StatefulWidget> createState() => _CityPage(this._city, this._favorite, this._favCities);
}
