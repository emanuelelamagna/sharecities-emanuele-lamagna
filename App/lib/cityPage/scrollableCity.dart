import 'package:flutter/cupertino.dart';

class ScrollableCity extends StatelessWidget   {

  final String _image;
  final ScrollController _scrollController = new ScrollController();

  ScrollableCity(this._image);

  @override
  Widget build(final BuildContext context) {
    return Container(
      height: 500,
      child:
      SafeArea(
        child: NotificationListener(
          child: SingleChildScrollView(
            controller: this._scrollController,
            scrollDirection: Axis.horizontal,
            child: Center(
                child: Row(
                  children: <Widget>[
                    Image.asset(this._image),
                  ],
                )
            )
        )
        ),
      ),);
  }

}