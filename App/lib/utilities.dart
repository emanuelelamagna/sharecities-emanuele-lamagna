import 'dart:ui';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utilities {
	static const double FIELDS_WIDTH = 350;
	static const double FIELDS_HEIGHT = 37;
	static const double BUTTON_RADIUS = 30;
	static const double TEXT_LABELS_SIZE =  23;
	//static const String INTERNET_URL = "http://isi-studio8bis.csr.unibo.it:8094/";
  //static const String INTERNET_URL = "http://172.20.10.2:3000/";
  static const String INTERNET_URL = "http://192.168.1.126:3000/";
  //static const String INTERNET_URL = "http://localhost:3000/";
	static final double verticalRatio = window.physicalSize.height / 100;
	static final double horizontalRatio = window.physicalSize.width / 100;

	static void logout() async {
		await FlutterSecureStorage().delete(key: "jwt");
		final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
		sharedPreferences.clear();
	}
}