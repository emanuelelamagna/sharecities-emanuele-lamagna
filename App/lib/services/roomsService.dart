import 'package:applicazione_tesi/models/room.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utilities.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

class RoomsService {

  Future<List<Room>> attemptGetRooms(final String city, final String country) async {
    var res = await http.post(
      Utilities.INTERNET_URL + 'locals/rooms',
      body : {
        'country' : country,
        'city' : city
      }
    );
    if(res.statusCode == 200) {
      final List<Room> rooms = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        rooms.add(Room.fromJson(json.decode(res.body)[i]));
      }
      return rooms;
    } else {
      return null;
    }
  }

  Future<List<Room>> attemptGetCloseRooms(final String city, final String country, final Position position,
                                          final int distance) async {
    final latitude = position.latitude.toString();
    final longitude = position.longitude.toString();
    final url = Utilities.INTERNET_URL
        + 'rooms/getClosestRooms/'
        + city + '/'
        + country + '/'
        + latitude + '/'
        + longitude + '/'
        + distance.toString();

    var res = await http.get(url);

    if(res.statusCode == 200) {
      FlutterSecureStorage().write(key: 'closeRooms-'+city, value: res.body);
      final List<Room> rooms = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        rooms.add(Room.fromJson(json.decode(res.body)[i]));
      }
      return rooms;
    } else {
      return null;
    }
  }

  Future<Room> attemptGetMyRoom(final String email) async {
    final url = Utilities.INTERNET_URL + 'rooms/getRoom/';
    var res = await http.post(url, body: {
      'email' : email
    });
    if(res.statusCode == 200) {
      return Room.fromJson(json.decode(res.body));
    } else {
      return null;
    }
  }

  Future<String> attemptInsertRoom(final String contacts, final String work, final String interests,
                                    final bool tour, final String latitude, final String longitude,
                                    final String city, final String country) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String email = prefs.getString('email');
    final url = Utilities.INTERNET_URL + 'rooms/newRoom/';
    var res = await http.post(url, body: {
      'email' : email,
      'contacts' : contacts,
      'work' : work,
      'interests' : interests,
      'tour' : tour ? "1" : "",
      'latitude' : latitude,
      'longitude' : longitude,
      'city' : city,
      'country' : country
    });
    if(res.statusCode == 200) {
      return "";
    } else {
      return res.statusCode.toString();
    }
  }

  Future<String> attemptEditRoom(final String contacts, final String work, final String interests,
      final bool tour, final String latitude, final String longitude,
      final String city, final String country, final String roomID) async {
    print(tour);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final String email = prefs.getString('email');
    final url = Utilities.INTERNET_URL + 'rooms/editRoom/';
    var res = await http.post(url, body: {
      'room_id' : roomID.toString(),
      'jwt'   : jwt,
      'email' : email,
      'contacts' : contacts,
      'work' : work,
      'interests' : interests,
      'tour' : tour ? "1" : "",
      'latitude' : latitude,
      'longitude' : longitude,
      'city' : city,
      'country' : country
    });
    if(res.statusCode == 200) {
      return "";
    } else {
      return res.statusCode.toString();
    }
  }

  Future<String> attemptDeleteRoom(final String roomID) async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final String email = prefs.getString('email');
    final url = Utilities.INTERNET_URL + 'rooms/deleteRoom/';

    var res = await http.post(url, body: {
      'id_to_delete' : roomID,
      'jwt'   : jwt,
      'email' : email,
    });
    if(res.statusCode == 200) {
      return "";
    } else {
      return res.statusCode.toString();
    }
  }
}