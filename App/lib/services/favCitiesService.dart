import 'package:applicazione_tesi/models/city.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utilities.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

class FavCitiesService {

  Future<bool> attemptNewFavCity(final String city, final String country) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.getString('email');
    final url = Utilities.INTERNET_URL + 'cities/addFavCity/';
    var res = await http.post(url, body: {
      'email'   : email,
      'city'    : city,
      'country' : country
    });

    return res.statusCode == 200;
  }

  Future<List<City>> attemptGetFavCities(final String email) async {
    final url = Utilities.INTERNET_URL + 'cities/getFavCities/';
    var res = await http.post(url, body: {
      'email'   : email,
    });
    if(res.statusCode == 200) {
      final List<City> cities = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        cities.add(City.fromJson(json.decode(res.body)[i], 0));
      }
      return cities;
    } else {
      return null;
    }
  }
}