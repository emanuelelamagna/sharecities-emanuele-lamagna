import 'package:applicazione_tesi/models/review.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

import '../utilities.dart';

class ReviewService {

  Future<String> attemptNewReview(final Room room, final String text) async {
   final SharedPreferences prefs = await SharedPreferences.getInstance();
   final String email = prefs.getString('email');
   final String name = prefs.getString('name');
   final String value = prefs.getString('reviewValue').toString();
   final url = Utilities.INTERNET_URL + 'reviews/newReview/';
   var res = await http.post(url, body: {
    'receiver' : room.emailUser,
    'email_sender' : email,
    'name_sender' : name,
    'text' : text,
    'value' : value,
   });
   if(res.statusCode == 200) {
    return "";
   } else {
    return res.statusCode.toString();
   }
  }

  Future<List<Review>> attemptGetReviews(final String roomID) async {
   final url = Utilities.INTERNET_URL + 'reviews/getReviewsInRoom/';
   var res = await http.post(url, body: {
    'room_id' : roomID,
   });
   if(res.statusCode == 200) {
    final List<Review> reviews = [];
    for(var i = 0; i < json.decode(res.body).length; i++) {
     reviews.add(Review.fromJson(json.decode(res.body)[i]));
    }
    return reviews;
   } else {
    return null;
   }
  }

 }