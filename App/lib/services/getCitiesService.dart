import 'dart:math';

import 'package:applicazione_tesi/models/city.dart';
import 'package:applicazione_tesi/models/freePlace.dart';
import 'package:applicazione_tesi/services/locationService.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import '../utilities.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json, jsonDecode;

class GetCitiesService {

  Future<List<City>> attemptGetCities() async {
    print("In attemptGetCities");
    final position = await LocationService().getCurrentPosition();
    //final position = await Geolocator.getCurrentPosition();

    var res = await http.get(Utilities.INTERNET_URL + 'locals/cities');
    if(res.statusCode == 200) {
      final List<City> cities = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        final double lat = json.decode(res.body)[i.toString()]['latitude'];
        final double lon = json.decode(res.body)[i.toString()]['longitude'];
        cities.add(City.fromJson(json.decode(res.body)[i.toString()],
                    this.getDistance(lat, lon, position.latitude, position.longitude)));
      }
      cities.sort((a, b) => (a.distance - b.distance).floor());
      return cities;
    } else {
      return null;
    }
  }

  Future<List<FreePlace>> attemptGetFreePlaces(final int cityID) async {
    var res = await http.get(Utilities.INTERNET_URL + 'cities/getFreePlaces/' + cityID.toString());
    if(res.statusCode == 200) {
      FlutterSecureStorage().write(key: 'freePlaces' + cityID.toString(), value: res.body);
      final List<FreePlace> freePlaces = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        freePlaces.add(FreePlace.fromJson(json.decode(res.body)[i]));
      }
      return freePlaces;
    } else {
      return null;
    }
  }

  double getDistance(final double cityLat, final double cityLon, final double latitude, final double longitude) {
    print("Dentro getDistance");
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((latitude - cityLat) * p)/2 + 
          c(cityLat * p) * c(latitude * p) * 
          (1 - c((longitude - cityLon) * p))/2;
    print(12742 * asin(sqrt(a)));
    return 12742 * asin(sqrt(a));
  
  }
}