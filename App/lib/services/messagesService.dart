import 'package:applicazione_tesi/models/message.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utilities.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

class MessagesService {

  Future<String> attemptNewMessage(final Room room, final String text) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String email = prefs.getString('email');
    final String name = prefs.getString('name');
    final url = Utilities.INTERNET_URL + 'messages/newMessage/';
    var res = await http.post(url, body: {
      'receiver' : room.emailUser,
      'email_sender' : email,
      'name_sender' : name,
      'text' : text,
    });
    if(res.statusCode == 200) {
      return "";
    } else {
      return res.statusCode.toString();
    }
  }

  Future<List<Message>> attemptGetMessages(final String roomID) async {
    final url = Utilities.INTERNET_URL + 'messages/getMessagesInRoom/';
    var res = await http.post(url, body: {
      'room_id' : roomID,
    });
    if(res.statusCode == 200) {
      final List<Message> messages = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        messages.add(Message.fromJson(json.decode(res.body)[i]));
      }
      return messages;
    } else {
      return null;
    }
  }

  Future<String> attemptDeleteMessage(final String messageID) async {
    final url = Utilities.INTERNET_URL + 'messages/deleteMessage/';
    var res = await http.post(url, body: {
      'message_id' : messageID,
    });
    if(res.statusCode == 200) {
      return "";
    } else {
      return res.body;
    }
  }
}