import 'package:applicazione_tesi/models/user.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../utilities.dart';
import 'dart:convert' show json;

class UserService {

  final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();

  Future<String> attemptContactServer(String email, String password, String name/*, String surname*/) async {
    print(Utilities.INTERNET_URL + 'signup');
    final res =  await http.post(Utilities.INTERNET_URL + 'locals/signup', body: {
      'email'       : email,
      'password'    : password,
      'name'        : name,
      'type'        : "1",
    
      //'surname'     : surname,
    });
    print(res.statusCode);
    if (res.statusCode == 409) {
      return "Esiste già un account legato alla mail inserita.";
    }
    return "";
  }

  Future<User> attemptGetUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.get('email');
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final res =  await http.post(Utilities.INTERNET_URL + 'locals/localmail', body: {
      'email'       : email,
      'jwt'         : jwt,
    });
    if (res.statusCode == 404) {
      return null;
    }
    print(res.body);
    return User.fromJson(json.decode(res.body));
    //return User.fromValues(json.decode(res.body), email);
  }

  Future<String> attemptGetImageInfo(String localmail, String num) async {
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final res0 =  await http.post(Utilities.INTERNET_URL + 'locals/localmail', body: {
      'email'       : localmail,
      'jwt'         : jwt,
    });

    final userID = json.decode(res0.body)['id'];
    final res =  await http.post(Utilities.INTERNET_URL + 'images/imageinfo', body: {
      'localId'       : userID.toString(),
      'number'   : num,
      'jwt'         : jwt,
    });
    
    if (res.statusCode == 404) {
      return null;
    }
    if(json.decode(res.body)['image']==null || json.decode(res.body)['image']==""){
      return "No description available";
    }
    return json.decode(res.body)['image'];
  }

  Future<String> attemptUpdateUser(final String name, final String surname,
                                    final String oldPassword, final String password) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.get('email');
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final res =  await http.post(Utilities.INTERNET_URL + 'users/updateUser', body: {
      'email'       : email,
      'jwt'         : jwt,
      'name'        : name,
      'surname'     : surname,
      'password'    : password,
      'oldpassword' : oldPassword
    });
    if (res.statusCode != 200) {
      return "Errore.";
    }
    return "";
  }
}