import 'package:applicazione_tesi/cityPage/cityPage.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/models/city.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CityButton extends StatelessWidget {

  static const double _CONTAINER_HEIGHT = 120;

  final List<City> _favCities;
  final City _city;
  final bool _favorite;

  CityButton(this._city, this._favorite, this._favCities);


  @override
  Widget build(final BuildContext context) {
    return this._getCityButton(context);
  }


  Widget _getCityButton(final BuildContext context) {
    return Container(
        decoration: Decorations.itemDecorationContainer(),
        height: _CONTAINER_HEIGHT * 0.8, // height of the button
        width: Utilities.FIELDS_WIDTH * 0.9,
        child: FlatButton(onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => CityPage(this._city, this._favorite, this._favCities)));
        },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(children: <Widget>[
                        Text(this._city.name,
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey[200]),),
                        Text(" (" + this._city.distance.toStringAsFixed(0) + "Km)",
                          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.grey[200]),),
                      ],),
                    ],),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(height: _CONTAINER_HEIGHT * 0.7,
                          child: Image.asset('assets/images/cityLogo2.png')),
                    ],)
                ])
        ));
  }
}