import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/normalText.dart';
import 'package:applicazione_tesi/listRoomsPage/roomButton.dart';
import 'package:applicazione_tesi/models/city.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/services/roomsService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:geocoder/geocoder.dart';

import '../genericComponents/logoAndBackButton.dart';

class _ListRoomsPage extends State<ListRoomsPage> {

  final List<Room> _rooms = [];
  final City _city;

  _ListRoomsPage(this._city);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
            alignment: AlignmentDirectional.topCenter,
            children: <Widget>[
              Container(
              decoration: Decorations.imageLogin()),
              Center(
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        LogoAndBackButton(),
                        SizedBox(height: Utilities.verticalRatio * 0.6),
                        NormalText("Rooms in the city of " + this._city.name, false),
                        SizedBox(height: Utilities.verticalRatio * 0.6),
                        Container(
                            height: Utilities.verticalRatio * 24,
                            width: Utilities.FIELDS_WIDTH ,
                            decoration: Decorations.mainGreyDecorationContainer(),
                            child: Column(children: <Widget>[
                              Expanded(
                                child: Align(
                                alignment: Alignment.bottomCenter,
                                  child: Container(
                                  height: Utilities.verticalRatio * 50,
                                    width: Utilities.FIELDS_WIDTH * 2,
                                    child:  SingleChildScrollView(
                                        child: FutureBuilder(
                                            future: this._getRooms(),
                                            builder: (context, snapshot) => snapshot.hasData ?
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              mainAxisSize: MainAxisSize.max,
                                              children: <Widget>[
                                                this._rooms.length > 0 ?
                                                Container(width: Utilities.FIELDS_WIDTH * 0.9, child: ListView.separated(
                                                  itemCount: this._rooms.length,
                                                  physics: NeverScrollableScrollPhysics(),
                                                  shrinkWrap: true, itemBuilder: (context, index) {
                                                    return RoomButton(this._rooms[index], this._rooms);
                                                },
                                                  separatorBuilder: (context, index) => SizedBox(height: Utilities.verticalRatio,),
                                                ),
                                                ) :
                                                NormalText("There are no rooms yet.", true)
                                              ],) : snapshot.hasError ? Text("Error loading data.") : LinearProgressIndicator()
                                        )
                                    ))))
                            ],)
                        ),
                        SizedBox(height: Utilities.verticalRatio * 0.8,),
                        /*SecondaryButton("Vai alla città", () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => CityPage()));
                        }),

                         */
                        SizedBox(height: Utilities.verticalRatio * 3.5),
                        Text(
                          "Sort locals by:",
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 22, 
                            fontWeight: FontWeight.bold, 
                            color: Colors.white
                          ),
                        ),
                        SizedBox(height: Utilities.verticalRatio * 2.5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SecondaryButton(
                              "Interests", (){
                                
                              }
                            ),
                            SizedBox(width: Utilities.verticalRatio * 1),
                            SecondaryButton(
                              "New locals", (){
                                setState(() {
                                  print("Ciao da bottone new locals");
                                });
                              }
                            ),
                            SizedBox(width: Utilities.verticalRatio * 1),
                            SecondaryButton(
                              "Position", (){
                                
                              }
                            ),
                          ]
                        ),
                      ]
                  )
              )
        ]));
  }

  Future <String> _getRooms() async {
    final rooms = await RoomsService().attemptGetRooms(this._city.name, this._city.country);
    if (rooms != null) {
      this._rooms.clear();
      this._rooms.addAll(rooms);
    }
    final query = "Piazza Gabriele Goidanich, 60, Cesena";
    var addresses = await Geocoder.local.findAddressesFromQuery(query);
    var first = addresses.first;
    print("${first.featureName} : ${first.coordinates}");
    return "";
  }

}

class ListRoomsPage extends StatefulWidget {

  final City _city;

  ListRoomsPage(this._city);

  @override
  State<StatefulWidget> createState() => _ListRoomsPage(this._city);
}