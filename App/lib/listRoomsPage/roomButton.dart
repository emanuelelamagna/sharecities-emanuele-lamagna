import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/roomPage/roomPage.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class RoomButton extends StatelessWidget {

  static const double _CONTAINER_HEIGHT = 120;

  final Room _room;
  final List<Room> _rooms;

  String imagePath;

  RoomButton(this._room, this._rooms);

  @override
  Widget build(final BuildContext context) {
    return this._getRoomButton(context);
  }

  Widget _getRoomButton(final BuildContext context) {
    if (this._rooms.indexOf(this._room) == 0){
      imagePath = "5";
    } else if(this._rooms.indexOf(this._room) == 1){
      imagePath = "3";
    } else {
      imagePath = "1";
    }
    return Container(
        decoration: Decorations.itemDecorationContainer(),
        height: _CONTAINER_HEIGHT * 0.8, // height of the button
        width: Utilities.FIELDS_WIDTH * 0.9,
        child: FlatButton(onPressed: () {
          print(this._room);
          Navigator.push(context, MaterialPageRoute(builder: (context) => RoomPage(this._room, this._rooms)));
        },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "Room of\n " + this._room.name ,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white),),
                    ],),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      //Container(height: _CONTAINER_HEIGHT * 0.3,
                      Container(height: _CONTAINER_HEIGHT * 0.5,
                          child: Image.asset('assets/images/cityLogo2.png')),
                          //child: Image.asset('assets/images/heart'+imagePath+'.png')),
                    ],)
                ])
        ));
  }
}