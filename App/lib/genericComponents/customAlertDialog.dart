import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utilities.dart';

class CustomAlertDialog {

  final BuildContext _context;
  final String _text;
  final String _title;

  CustomAlertDialog(this._context, this._title, this._text);

  void show() {
    showDialog(context: this._context,
    builder: (final BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Utilities.BUTTON_RADIUS)),
        title: Text(
          this._title,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
        ),
        content: Text(
          this._text,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 19),
        ),
        actions: <Widget>[
          FlatButton(
              child: Text(
                'OK',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                Navigator.of(this._context).pop();
              }),
        ],
      );
    });
  }

}
