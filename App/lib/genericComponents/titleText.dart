import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {

  final _text;
  final TextAlign _align;
  final bool _dark;

  TitleText(this._text, this._align, this._dark);

  @override
  Widget build(final BuildContext context) {
    return Text(
      this._text,
      textAlign: this._align,
      style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 30,
          color: this._dark ? Colors.grey[800] : Colors.grey[300]
      ),
    );
  }

}