import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NormalText extends StatelessWidget {

  final _text;
  final bool _dark;

  NormalText(this._text, this._dark);

  @override
  Widget build(final BuildContext context) {
    return Text(
      this._text,
      textAlign: TextAlign.center,
      overflow: TextOverflow.fade,
      style: TextStyle(
          fontSize: 20,
          color:this._dark ? Colors.grey[800] : Colors.grey[300]
      ),
    );
  }

}