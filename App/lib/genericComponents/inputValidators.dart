import 'package:email_validator/email_validator.dart';

class InputValidators {

  static String nameValidator(final String value) {
    if (value.length > 30) {
      return "Max length 30 characters.";
    } else if (value.isEmpty) {
      return "Please enter a first name.";
    }
    return null;
  }

  static String surnameValidator(final String value) {
    if (value.length > 30) {
      return "Max length 30 characters.";
    } else if (value.isEmpty) {
      return "Please enter a surname.";
    }
    return null;
  }

  static String emailValidator(final String value) {
    if (!EmailValidator.validate(value)) {
      return "Please enter a valid email.";
    }
    return null;
  }

  static String passwordValidator(final String value) {
    final String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$';
    final RegExp regExp = new RegExp(pattern);
    if(regExp.hasMatch(value)) {
      return null;
    }
    return "Upper case, lower case and number required \nand must have at least 6 characters.";
  }

  static String beachNumberValidator(final String value) {
    if (value.length > 3) {
      return "Max length 3 characters.";
    } else if (value.isEmpty) {
      return "Inserire il numero della spiaggia.";
    }
    return null;
  }

  static String cityValidator(final String value) {
    if (value.length > 30) {
      return "Max length 30 characters.";
    } else if (value.isEmpty) {
      return "Inserire la città della spiaggia.";
    }
    return null;
  }

  static String beachNameValidator(final String value) {
    if (value.length > 30) {
      return "Lunghezza max. 30 caratteri.";
    } else if (value.isEmpty) {
      return "Inserire il nome della spiaggia.";
    }
    return null;
  }

  static String numShadowsValidator(final String value) {
    if (value.isEmpty ||value.length > 2 || int.parse(value) <= 0 || int.parse(value) > 30) {
      return "Inserire un numero (max. 30).";
    }
    return null;
  }

  static String codeValidator(final String value) {
    if (value.length != 6) {
      return "Inserire il codice di 6 cifre.";
    }
    return null;
  }
}