import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Logo extends StatelessWidget {

  @override
  Widget build(final BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Flexible(
        child: Image.asset(
          'assets/images/logo.png',
          width: 300,//main logo
          height: 45,
        ),
      ),
    ]);
  }

}