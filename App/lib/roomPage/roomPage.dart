import 'package:applicazione_tesi/env.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/genericComponents/normalText.dart';
import 'package:applicazione_tesi/genericComponents/titleText.dart';
import 'package:applicazione_tesi/models/message.dart';
import 'package:applicazione_tesi/models/review.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/roomPage/buttonImageRoom.dart';
import 'package:applicazione_tesi/roomPage/reviewContainer.dart';
import 'package:applicazione_tesi/roomPage/starReview.dart';
import 'package:applicazione_tesi/services/messagesService.dart';
import 'package:applicazione_tesi/services/userService.dart';
import 'package:applicazione_tesi/services/reviewService.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:panorama/panorama.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'messageContainer.dart';

class _RoomPage extends State<RoomPage> {
  static final double _iconSize = 60;
  static final double _spaceSize = 10;

  final Room _room;
  final List<Room> _rooms;

  final TextEditingController _messageController = TextEditingController();
  final TextEditingController _reviewController = TextEditingController();
  final ScrollController _scrollControllerMessages = ScrollController();
  final ScrollController _scrollController = new ScrollController();
  int _itemSelected = 0;
  double _infoWidth = 0;
  double _infoHeight = 0;
  String _title = "";
  String _text = "";
  String _myMail = "";
  String _imageText = "";
  String _infosText = "Tap a picture if you want to read the description";
  List<Message> _messages = [];
  List<Review> _reviews = [];
  int _indexReviewDone = -1;
  //int indexToGo = -1;
  int indexToGo = -1;
  int lastIndex = -1;
  //String similarity = "";

  _RoomPage(this._room, this._rooms);

  @override
  Widget build(BuildContext context) {
    print("Prima");
    print(this._room.emailUser);
    print("Dopo");
    //indexToGo = this._rooms.indexOf(this._room) -1;
    indexToGo = this._rooms.indexOf(this._room) +1;
    /*if (this._rooms.indexOf(this._room) == 0){
      similarity = "5";
    } else if(this._rooms.indexOf(this._room) == 1){
      similarity = "3";
    } else {
      similarity = "1";
    }*/
    return Scaffold(
        body: GestureDetector(
      child: Stack(
        children: <Widget>[
          env.flavor == BuildFlavor.panorama
              ?
              //versione panorama immersiva
              Panorama(
                  //isCompass: true
                  animSpeed: 0.0,
                  sensitivity: 2,
                  interactive: true,
                  onTap: (longitude, latitude, tilt) => picturePressed(longitude, latitude),
                  //child: Image.asset(this._room.imagePath),)
                  //child: Image.network(Utilities.INTERNET_URL+this._room.imagePath)
                  child: Image(image: CachedNetworkImageProvider(Utilities.INTERNET_URL + "images/" + this._room.emailUser + ".png"))
                  
                )
              :
              //versione statica scorrevole
              
              SafeArea(
                  child: NotificationListener(
                      child: SingleChildScrollView(
                          controller: this._scrollController,
                          scrollDirection: Axis.horizontal,
                          child: Center(
                              child: Row(
                            children: <Widget>[
                              //Image.asset(this._room.imagePath),
                              Image(image: CachedNetworkImageProvider(Utilities.INTERNET_URL + "images/" + this._room.emailUser + ".png"))
                            ],
                          )))),
                ),
                
          Padding(
              padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      SizedBox(
                        height: _spaceSize,
                      ),
                      FlatButton(
                          onPressed: () {
                            this._showInfo(8);
                          },
                          child: ButtonImageRoom(
                              'assets/images/info.png', _iconSize)),
                      SizedBox(
                        height: _spaceSize,
                      ),
                      FlatButton(
                          onPressed: () {
                            this._showInfo(1);
                          },
                          child: ButtonImageRoom(
                              'assets/images/contact.png', _iconSize)),
                      SizedBox(
                        height: _spaceSize,
                      ),
                      FlatButton(
                          onPressed: () {
                            //this._showInfo(3);
                          },
                          child: ButtonImageRoom(
                              'assets/images/game.png', _iconSize)),
                      SizedBox(
                        height: _spaceSize,
                      ),
                      /*
                      FlatButton(
                          onPressed: () {
                            this._showInfo(4);
                          },
                          child: ButtonImageRoom(
                              'assets/images/tour.png', _iconSize)),
                      SizedBox(
                        height: _spaceSize,
                      ),
                      FlatButton(
                          onPressed: () {
                            this._showInfo(5);
                          },
                          child: ButtonImageRoom(
                              'assets/images/message.png', _iconSize)),
                      SizedBox(
                        height: _spaceSize,
                      ),
                      FlatButton(
                          onPressed: () {
                            this._showInfo(6);
                          },
                          child: ButtonImageRoom(
                              'assets/images/star.png', _iconSize)),
                              */
                    ],
                  ),
                  Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 30, 0),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: AnimatedContainer(
                            curve: Curves.easeInOutQuart,
                            duration: Duration(milliseconds: 500),
                            height: this._infoHeight,
                            width: this._infoWidth,
                            decoration:
                                Decorations.textBoxDecorationContainer(),
                            child: this._itemSelected == 5
                                ? Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: Utilities.verticalRatio,
                                      ),
                                      TitleText(
                                          this._title, TextAlign.center, true),
                                      HorizontalLine(Colors.grey[800],
                                          this._infoWidth * 0.85),
                                      Expanded(
                                          child: Align(
                                              alignment: Alignment.bottomCenter,
                                              child: Container(
                                                child: SingleChildScrollView(
                                                    controller: this
                                                        ._scrollControllerMessages,
                                                    child: FutureBuilder(
                                                        future:
                                                            this._getMessages(),
                                                        builder: (context,
                                                                snapshot) =>
                                                            snapshot.hasData
                                                                ? Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    mainAxisSize:
                                                                        MainAxisSize
                                                                            .max,
                                                                    children: <
                                                                        Widget>[
                                                                      this._messages.length >
                                                                              0
                                                                          ? Container(
                                                                              width: Utilities.FIELDS_WIDTH * 0.9,
                                                                              child: ListView.separated(
                                                                                itemCount: this._messages.length,
                                                                                physics: NeverScrollableScrollPhysics(),
                                                                                shrinkWrap: true,
                                                                                itemBuilder: (context, index) {
                                                                                  final message = this._messages.elementAt(index);
                                                                                  final isMe = this._myMail == message.emailSender;
                                                                                  final isOwner = this._room.emailUser == message.emailSender;
                                                                                  return MessageContainer(this._infoWidth * 0.8, message, isMe, isOwner);
                                                                                },
                                                                                separatorBuilder: (context, index) => SizedBox(
                                                                                  height: 0,
                                                                                ),
                                                                              ),
                                                                            )
                                                                          : Text(
                                                                              ''),
                                                                    ],
                                                                  )
                                                                : snapshot
                                                                        .hasError
                                                                    ? Text(
                                                                        "Error loading data.")
                                                                    : LinearProgressIndicator())),
                                              ))),
                                      Container(
                                          width: this._infoWidth * 0.9,
                                          decoration: Decorations
                                              .textBoxDecorationContainer(),
                                          child: Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width:
                                                    Utilities.horizontalRatio,
                                              ),
                                              Expanded(
                                                  child: TextField(
                                                controller:
                                                    this._messageController,
                                                decoration:
                                                    InputDecoration.collapsed(
                                                        hintText: "Write.."),
                                              )),
                                              IconButton(
                                                icon: Icon(Icons.send),
                                                iconSize: 25,
                                                color: Colors.lightBlue[700],
                                                onPressed: () {
                                                  this._sendNewMessage();
                                                },
                                              )
                                            ],
                                          )),
                                      SizedBox(
                                        height: Utilities.verticalRatio,
                                      ),
                                    ],
                                  )
                                : this._itemSelected == 6
                                    ? Column(
                                        children: <Widget>[
                                          SizedBox(
                                            height: Utilities.verticalRatio,
                                          ),
                                          TitleText(this._title,
                                              TextAlign.center, true),
                                          HorizontalLine(Colors.grey[800],
                                              this._infoWidth * 0.85),
                                          Expanded(
                                              child: Align(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  child: Container(
                                                    child: SingleChildScrollView(
                                                        child: FutureBuilder(
                                                            future: this._getReviews(),
                                                            builder: (context, snapshot) => snapshot.hasData
                                                                ? Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    mainAxisSize:
                                                                        MainAxisSize
                                                                            .max,
                                                                    children: <
                                                                        Widget>[
                                                                      this._reviews.length >
                                                                              0
                                                                          ? Container(
                                                                              width: Utilities.FIELDS_WIDTH * 0.9,
                                                                              child: ListView.separated(
                                                                                itemCount: this._reviews.length,
                                                                                physics: NeverScrollableScrollPhysics(),
                                                                                shrinkWrap: true,
                                                                                itemBuilder: (context, index) {
                                                                                  final review = this._reviews.elementAt(index);
                                                                                  final isMe = this._myMail == review.emailSender;
                                                                                  return ReviewContainer(this._infoWidth * 0.8, review, isMe);
                                                                                },
                                                                                separatorBuilder: (context, index) => SizedBox(
                                                                                  height: 0,
                                                                                ),
                                                                              ),
                                                                            )
                                                                          : Text(
                                                                              ''),
                                                                    ],
                                                                  )
                                                                : snapshot.hasError
                                                                    ? Text("Error loading data.")
                                                                    : LinearProgressIndicator())),
                                                  ))),
                                          Container(
                                              width: this._infoWidth * 0.94,
                                              height:
                                                  Utilities.verticalRatio * 5,
                                              decoration: Decorations
                                                  .textBoxDecorationContainer(),
                                              child: Column(
                                                children: <Widget>[
                                                  StarReview(this
                                                              ._indexReviewDone >
                                                          -1
                                                      ? this._reviews.elementAt(
                                                          this._indexReviewDone)
                                                      : null),
                                                  Row(
                                                    children: <Widget>[
                                                      SizedBox(
                                                        width: Utilities
                                                            .horizontalRatio,
                                                      ),
                                                      Expanded(
                                                          child: TextField(
                                                        controller: this
                                                            ._reviewController,
                                                        decoration: InputDecoration
                                                            .collapsed(
                                                                hintText: this
                                                                            ._indexReviewDone !=
                                                                        -1
                                                                    ? "Modify review..."
                                                                    : "Write.."),
                                                      )),
                                                      IconButton(
                                                        icon: Icon(Icons.send),
                                                        iconSize: 25,
                                                        color: Colors
                                                            .lightBlue[700],
                                                        onPressed: () {
                                                          this._sendReview();
                                                        },
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              )),
                                          SizedBox(
                                            height: Utilities.verticalRatio,
                                          ),
                                        ],
                                      )
                                    : Column(children: <Widget>[
                                        SizedBox(
                                          height: Utilities.verticalRatio,
                                        ),
                                        TitleText(this._title, TextAlign.center,
                                            true),
                                        HorizontalLine(Colors.grey[800],
                                            this._infoWidth * 0.85),
                                        SizedBox(
                                          height: Utilities.verticalRatio,
                                        ),
                                        NormalText(this._text, true),
                                      ])),
                      ))
                ],
              )),
          Positioned(
            bottom: 0,
            right: 0,
            child: FlatButton(
                onPressed: () async {
                  //if Luca(3) exit
                  //if not luca's room and the room before was before in the list
                  if (indexToGo != 3 && lastIndex < indexToGo) {
                    lastIndex = indexToGo;
                    bool toPop = await Navigator.push(
                        context,
                        MaterialPageRoute<bool>(
                            builder: (context) =>
                                RoomPage(this._rooms[indexToGo], this._rooms)));
                    if(toPop) {
                      Navigator.pop(context, true);
                    }
                  } else {
                    if (indexToGo != lastIndex) {
                      Navigator.pop(context, true);
                    } else {
                      bool toPop = await Navigator.push(
                          context,
                          MaterialPageRoute<bool>(
                              builder: (context) =>
                                  RoomPage(this._rooms[indexToGo], this._rooms)));
                      if(toPop) {
                        Navigator.pop(context, true);
                      }
                    }
                  }
                },
                child:
                    ButtonImageRoom('assets/images/icon_door.png', _iconSize)),
          ),
          /*Positioned(
            top: 50,
            right: 0,
            height: 40,
            child: ButtonImageRoom('assets/images/heart'+similarity+'.png', _iconSize),
          ),*/
          MediaQuery.of(context).viewInsets.bottom == 0
              ? Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: SecondaryButton('Back', () {
                      Navigator.pop(context, true);
                    }),
                  ))
              : Text(''),
        ],
      ),
    ));
  }

  void picturePressed(var long, var lat) async{
    final mail = _room.emailUser;
    //print('on tap: $long, $lat');
    if(long > -129 && long < -115 && lat > -8.5 && lat < 11.5){
      print("Dentro quadro 1");
      this._imageText = await UserService().attemptGetImageInfo((mail).toString(), "1");
      this._showInfo(7);
    } else if(long > -110 && long < -95.5 && lat > -7.5 && lat < 26){
      print("Dentro quadro 2");
      this._imageText = await UserService().attemptGetImageInfo((mail).toString(), "2");
      this._showInfo(7);
    } else if(long > -91.5 && long < -82.5 && lat > 6 && lat < 22){
      print("Dentro quadro 3");
      this._imageText = await UserService().attemptGetImageInfo((mail).toString(), "3");
      this._showInfo(7);
    } else if(long > 146 && long < 160 && lat > 7.5 && lat < 25.5){
      print("Dentro quadro 4");
      this._imageText = await UserService().attemptGetImageInfo((mail).toString(), "4");
      this._showInfo(7);
    } else if(long > -34.2 && long < -11 && lat > 9.5 && lat < 23.5){
      print("Dentro quadro 5");
      this._imageText = await UserService().attemptGetImageInfo((mail).toString(), "5");
      this._showInfo(7);
    }
    
  }

  void _sendNewMessage() {
    if (this._messageController.text != '') {
      if (this._messageController.text.length < 1000) {
        MessagesService()
            .attemptNewMessage(this._room, this._messageController.text)
            .then((result) {
          if (result.isEmpty) {
            this._disableKeyboard(context);
            this._messageController.text = "";
            this.setState(() {
              this._scrollUntilTheEnd();
            });
          } else {
            CustomAlertDialog(context, "Error", result).show();
          }
        });
      } else {
        CustomAlertDialog(context, "Message too long", "Max 1000 characters.")
            .show();
      }
    }
  }

  void _sendReview() async {
    final text = this._reviewController.text;

    if (text != '') {
      if (text.length < 1000) {
        ReviewService().attemptNewReview(this._room, text).then((result) {
          if (result.isNotEmpty) {
            CustomAlertDialog(context, "Error", result).show();
          } else {
            this._disableKeyboard(context);
            this._reviewController.text = "";
            this.setState(() {
              this._scrollUntilTheEnd();
            });
          }
        });
      } else {
        CustomAlertDialog(context, "Review too long", "Max 1000 characters.")
            .show();
      }
    }
  }

  void _scrollUntilTheEnd() {
    if (_scrollControllerMessages.hasClients) {
      this._scrollControllerMessages.animateTo(
            this._scrollControllerMessages.position.maxScrollExtent,
            curve: Curves.easeInOutQuint,
            duration: const Duration(milliseconds: 500),
          );
    }
  }

  void _showInfo(final int selection) {
    if (selection == this._itemSelected) {
      this._itemSelected = -1;
      this.setState(() {
        this._text = "";
        this._title = "";
        this._infoHeight = 0;
        this._infoWidth = 0;
      });
    } else if (selection != this._itemSelected) {
      this._itemSelected = selection;
      this.setState(() {
        this._infoHeight = 530;
        this._infoWidth = 220;
        this._updateTitles();
        if (this._room != null) {
          this._updateText();
        }
      });
    }
  }

  void openOtherRoom() {}

  void _updateTitles() {
    switch (this._itemSelected) {
      case 1:
        this._title = "Contacts";
        break;
      case 2:
        this._title = "Work";
        break;
      case 3:
        this._title = "Interests";
        break;
      case 4:
        this._title = "Services";
        break;
      case 5:
        this._title = "Messages";
        break;
      case 6:
        this._title = "Reviews";
        break;
      case 7:
        this._title = "Image";
        break;
      case 8:
        this._title = "Infos";
        break;
    }
  }

  void _updateText() {
    switch (this._itemSelected) {
      case 1:
        this._text = this._room.contacts;
        break;
      case 2:
        this._text = this._room.work;
        break;
      case 3:
        this._text = this._room.interests;
        break;
      case 4:
        this._text = this._room.tour /*== "1"
            ? 'The user offers the possibility to take guided tours, contact him to find out more!'
            : 'The user does not take guided tours.'*/;
        break;
      case 5:
        this._text = "";
        break;
      case 6:
        this._text = "";
        break;
      case 7:
        this._text = this._imageText;
        break;
      case 8:
        this._text = this._infosText;
        break;
    }
  }

  void _disableKeyboard(context) {
    final FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  Future<String> _getMessages() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    this._myMail = prefs.getString('email');
    final messages =
        await MessagesService().attemptGetMessages(this._room.roomID);
    if (messages != null) {
      this._messages.clear();
      this._messages.addAll(messages);
    }
    SchedulerBinding.instance.addPostFrameCallback((_) {
      this._scrollUntilTheEnd();
    });
    return "";
  }

  Future<String> _getReviews() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    this._myMail = prefs.getString('email');
    final reviews = await ReviewService().attemptGetReviews(this._room.roomID);
    if (reviews != null && this._title.contains("Reviews")) {
      this._reviews.clear();
      this._reviews.addAll(reviews);
      final myReview =
          reviews.where((elem) => elem.emailSender == this._myMail);
      this.setState(() {
        this._title = 'Reviews: ' + this._getReviewsAverage();
        if (myReview.length > 0) {
          this._indexReviewDone = this._reviews.indexOf(myReview.first);
        }
      });
    }
    SchedulerBinding.instance.addPostFrameCallback((_) {
      this._scrollUntilTheEnd();
    });

    return "";
  }

  String _getReviewsAverage() {
    return (this
                ._reviews
                .map((review) => review.value)
                .reduce((a, b) => a + b) /
            this._reviews.length)
        .toStringAsFixed(1);
  }
}

class RoomPage extends StatefulWidget {
  final Room _room;
  final List<Room> _rooms;

  RoomPage(this._room, this._rooms);

  @override
  State<StatefulWidget> createState() => _RoomPage(this._room, this._rooms);
}
