import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class _InfoContainer extends State<InfoContainer> {

  double _infoHeight = 0;
  double _infoWidth = 0;

  @override
  Widget build(final BuildContext context) {
    return AnimatedContainer(
      curve: Curves.easeInOutQuart,
      duration: Duration(milliseconds: 500),
      height: this._infoHeight,
      width: this._infoWidth,
      decoration: Decorations.itemDecorationContainer(),

    );
  }

}


class InfoContainer extends StatefulWidget {

  @override
State<StatefulWidget> createState() => _InfoContainer();
}