import 'package:applicazione_tesi/genericComponents/confirmationAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/models/message.dart';
import 'package:applicazione_tesi/services/messagesService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utilities.dart';

class MessageContainer extends StatelessWidget {

  final double _width;
  final Message _message;
  final bool _isMe;
  final bool _isOwner;

  MessageContainer(this._width, this._message, this._isMe, this._isOwner);

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
        onLongPress: () {
          if (this._isMe) {
            ConfirmationAlertDialog(this._deleteMessage, "Attention",
                "Are you sure you want to delete the message?", context).show();
          }
        },
        child:Column(
        children: <Widget>[
          Container(
            decoration: this._isMe ? Decorations.message()
                                   : this._isOwner ? Decorations.messageOwner()
                                                   : Decorations.containerLogin(),
            child: Padding(
                padding: EdgeInsets.fromLTRB(13, 10, 13, 10),
                child: Column(children: <Widget>[
                  Container(
                    width: this._width,
                    child: Text(
                      this._message.nameSender,
                      style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                      textAlign: this._isMe ? TextAlign.right : TextAlign.left, // has impact
                    ),
                  ),
                  Container(
                    width: this._width,
                    child: Text(
                      this._message.text,
                      style: TextStyle(fontSize: 17),
                      textAlign: this._isMe ? TextAlign.right : TextAlign.left, // has impact
                    ),
                  ),
                  Container(
                    width: this._width,
                    child: Text(
                      this._getTime(),
                      style: TextStyle(fontSize: 11, fontStyle: FontStyle.italic),
                      textAlign: this._isMe ? TextAlign.right : TextAlign.left, // has impact
                    ),
                  ),
                ])),
          ),
          SizedBox(height: Utilities.verticalRatio * 0.7,),
        ],
    ));
  }

  void _deleteMessage(final BuildContext context) {
    MessagesService().attemptDeleteMessage(this._message.messageID).then((result) {
      if (result.isEmpty) {
        Navigator.pop(context);
      } else {
        CustomAlertDialog(context, "Error", result).show();
      }
    });
  }

  String _getTime() {
    final date = this._message.time.split(' ').first;
    final time = this._message.time.split(' ').elementAt(1).split(':');
    return time[0] + ':' + time[1] + ", " +  date.split('-').elementAt(2) + '/' + date.split('-').elementAt(1);
  }
}
