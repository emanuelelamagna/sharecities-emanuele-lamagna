import 'dart:ui';
import 'package:applicazione_tesi/genericComponents/buttons/mainButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/customTextField.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/genericComponents/inputValidators.dart';
import 'package:applicazione_tesi/genericComponents/titleText.dart';
import 'package:applicazione_tesi/loginPage/loginPage.dart';
import 'package:applicazione_tesi/services/internetService.dart';
import 'package:applicazione_tesi/services/userService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../genericComponents/logoAndBackButton.dart';
import '../utilities.dart';

class _RegisterPage extends State<RegisterPage> {

  static const int _NAME_INDEX = 0;
  static const int _SURNAME_INDEX = 1;
  static const int _EMAIL_INDEX = 2;
  static const int _PASSWORD_INDEX = 3;
  static const int _CONF_PASSWORD_INDEX = 4;
  final List<TextEditingController> _controllers = [TextEditingController(), TextEditingController(), TextEditingController(),
                                                    TextEditingController(), TextEditingController()];
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(final BuildContext context) {

    return Scaffold(
        body: Stack(children: <Widget>[
          Container(decoration: Decorations.imageLogin()),
          SingleChildScrollView(
            child: Form(autovalidate: true, key: this._formKey, child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: Utilities.verticalRatio * 3), //space between the fields
                LogoAndBackButton(),
                SizedBox(
                  height: Utilities.verticalRatio * 1.5,
                ),
                Container(
                    decoration: Decorations.mainGreyDecorationContainer(),
                    width: Utilities.horizontalRatio * 33,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: Utilities.verticalRatio,),
                        TitleText('Registration', TextAlign.center, true),
                        HorizontalLine(Colors.grey[600], Utilities.FIELDS_WIDTH),
                        SizedBox(
                          height: Utilities.verticalRatio,
                        ),
                        CustomTextField(
                            false,
                            this._controllers.elementAt(_NAME_INDEX),
                            TextInputType.text,
                                (value) { return InputValidators.nameValidator(value); },
                            "First name"
                        ),
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            false,
                            this._controllers.elementAt(_SURNAME_INDEX),
                            TextInputType.text,
                                (value) { return InputValidators.surnameValidator(value); },
                            "Surname"
                        ), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            false,
                            this._controllers.elementAt(_EMAIL_INDEX),
                            TextInputType.emailAddress,
                                (value) { return InputValidators.emailValidator(value); },
                            "E-mail"
                        ), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            true,
                            this._controllers.elementAt(_PASSWORD_INDEX),
                            TextInputType.text, (value) { return InputValidators.passwordValidator(value); },
                            "Password"), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            true,
                            this._controllers.elementAt(_CONF_PASSWORD_INDEX),
                            TextInputType.text, (value) { return InputValidators.passwordValidator(value); },
                            "Confirm password"), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        MainButton("Create account", this._functionNewAccountButton, 150),
                        SizedBox(height: Utilities.verticalRatio),
                      ],
                    )),
                SizedBox(height: Utilities.verticalRatio * 2), //space between the fields
              ],
            ),
            ),
          )
        ],));
  }

  void _functionNewAccountButton() {
    InternetService.checkConnectivity().then((isConnected) {
      if (isConnected) {
        if (this._formKey.currentState.validate()) {
          this._checkInput(context);
        }
      } else {
        CustomAlertDialog(context, "Attention", "Check internet connection.").show();
      }
    });
  }

  void _checkInput(final BuildContext context) {

    bool aFieldIsEmpty = false;

    for (var controller in this._controllers) {
      if (controller.text == "") {
        CustomAlertDialog(context, "Error", "Fill in ALL the fields and check that they are correct.").show();
        this._disableKeyboard(context);
        aFieldIsEmpty = true;
        break;
      }
    }

    if (!aFieldIsEmpty) {
      final email = this._controllers[_EMAIL_INDEX].text;
      final password = this._controllers[_PASSWORD_INDEX].text;
      final name = this._controllers[_NAME_INDEX].text + " " + this._controllers[_SURNAME_INDEX].text;

      //final name = this._controllers[_NAME_INDEX].text;
      //final surname = this._controllers[_SURNAME_INDEX].text;

      UserService().attemptContactServer(email, password, name).then((result) {
        if (result == "") {
          this._showSuccessDialog();
        } else {
          CustomAlertDialog(context, "Error", result).show();
        }
      });
    }
  }

  void _showSuccessDialog() {
    showDialog(
        context: context,
        builder: (final BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15)),
            title: Text(
              "Profile created",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
            ),
            content: Text(
              "Account created successfully!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 19,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text(
                    'OK',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                LoginPage()
                        )
                    );
                  }),
            ],
          );
        });
  }

  void _disableKeyboard(context) {
    final FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

}

class RegisterPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _RegisterPage();
}
