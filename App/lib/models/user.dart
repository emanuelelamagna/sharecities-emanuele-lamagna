
class User {

  String _id;
  String _name;
  String _surname;
  String _email;
  String _interests;

  User(this._id, this._name, this._surname, this._email, this._interests);

  String get id => _id;

  String get name => _name;

  String get surname => _surname;

  String get email => _email;

  String get interests => _interests;

  factory User.fromJson(final dynamic json) {
    return User(json['id'], json['name'] as String, "", json['email'] as String, json['interests'] as String);
  }

  factory User.fromValues(final String name, final String email) {
    return User("", name, "", email, "");
  }

}