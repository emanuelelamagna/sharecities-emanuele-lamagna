
class Review {

  String _reviewID;
  String _nameSender;
  String _emailSender;
  String _text;
  String _roomID;
  int _value;

  Review(this._reviewID, this._nameSender, this._emailSender, this._text, this._roomID, this._value);

  String get reviewID => _reviewID;

  String get nameSender => _nameSender;

  String get emailSender => _emailSender;

  String get text => _text;

  String get roomID => _roomID;

  int get value => _value;

  factory Review.fromJson(final dynamic json) {
    return Review(json['id'] as String, json['name_sender'] as String,
        json['email_sender'] as String, json['text'] as String,
        json['room_id'] as String, int.parse(json['value']));
  }
}