import 'dart:ui';
import 'package:applicazione_tesi/genericComponents/buttons/mainButton.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/customTextField.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/myDataPage/myDataPage.dart';
import 'package:applicazione_tesi/services/internetService.dart';
import 'package:applicazione_tesi/services/locationService.dart';
import 'package:applicazione_tesi/services/roomsService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import '../genericComponents/logoAndBackButton.dart';
import '../utilities.dart';

class _NewRoomPage extends State<NewRoomPage> {

  static const int _CONTACTS_INDEX = 0;
  static const int _WORK_INDEX = 1;
  static const int _INTERESTS_INDEX = 2;

  final List<TextEditingController> _controllers = [TextEditingController(), TextEditingController(), TextEditingController()];
  final _formKey = GlobalKey<FormState>();
  Position _position;
  String _address = "";
  bool _tour = false;

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
        body: Stack(children: <Widget>[
          Container(decoration: Decorations.imageLogin()),
          SingleChildScrollView(
            child: Form(autovalidate: true, key: this._formKey, child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: Utilities.verticalRatio * 3), //space between the fields
                LogoAndBackButton(),
                SizedBox(
                  height: Utilities.verticalRatio * 1.5,
                ),
                Container(
                    decoration: Decorations.mainGreyDecorationContainer(),
                    width: Utilities.horizontalRatio * 33,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: Utilities.verticalRatio,),
                        Text("New room", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.grey[800]),),
                        HorizontalLine(Colors.grey[600], Utilities.FIELDS_WIDTH),
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            false,
                            this._controllers.elementAt(_CONTACTS_INDEX),
                            TextInputType.text,
                                (value) { return null; },
                            "Contacts"
                        ), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            false,
                            this._controllers.elementAt(_WORK_INDEX),
                            TextInputType.text,
                                (value) { return null; },
                            "Work"
                        ), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        CustomTextField(
                            false,
                            this._controllers.elementAt(_INTERESTS_INDEX),
                            TextInputType.text, (value) { return null; },
                            "Interests"), //name text field
                        SizedBox(
                            height:
                            Utilities.verticalRatio * 2), //space between the fields
                        SecondaryButton("Current position", () {
                            LocationService().getCurrentAddress().then((res) {
                              this.setState(() {
                                this._address = res;
                              });
                            });
                            LocationService().getCurrentPosition().then((res) {
                              this._position = res;
                            });
                        }),
                        SizedBox(
                            height:
                            Utilities.verticalRatio), //space between the fields
                        Text(this._address),
                        SizedBox(height: Utilities.verticalRatio),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                          Text("Offer a guided tour"),
                          Checkbox(
                            value: this._tour,
                            onChanged: (final bool value) {
                              this.setState(() {
                                 this._tour = value;
                              });
                            },
                          ),
                        ],),
                        SizedBox(height: Utilities.verticalRatio),
                        MainButton("Add", this._functionNewAccountButton, 130),
                        SizedBox(height: Utilities.verticalRatio),
                      ],
                    )),
                SizedBox(
                    height: Utilities.verticalRatio * 2), //space between the fields
              ],
            ),
            ),
          )
        ],));
  }

  void _functionNewAccountButton() {
    InternetService.checkConnectivity().then((isConnected) {
      if (isConnected) {
        if (this._checkEmptyFields() && this._address != '') {
          final List<String> address = this._address.split(',');
          final String country = address.last.split(' ').last;
          final String city = address.elementAt(2).split(' ').elementAt(2);

          RoomsService().attemptInsertRoom(
              this._controllers.elementAt(_CONTACTS_INDEX).text,
              this._controllers.elementAt(_WORK_INDEX).text,
              this._controllers.elementAt(_INTERESTS_INDEX).text,
              this._tour,
              this._position.latitude.toString(),
              this._position.longitude.toString(),
              city,
              country
          ).then((res) {
            if (res.isNotEmpty) {
              CustomAlertDialog(context, "Attention", "The city you are in is not part of the app yet.").show(); //DA CAMBIARE COL NOME DELL'APP
            } else {
              this._showSuccessDialog();
            }
          });
        } else {
          CustomAlertDialog(context, "Attention", "Fill in all fields and take your position.").show();
        }
      } else {
        CustomAlertDialog(context, "Attention", "Check internet connection.").show();
      }
    });
  }

  void _showSuccessDialog() {
    showDialog(
        context: context,
        builder: (final BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15)),
            title: Text(
              "Room created",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
            ),
            content: Text(
              "Room created successfully!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 19,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text(
                    'OK',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                MyDataPage()
                        )
                    );
                  }),
            ],
          );
        });
  }

  bool _checkEmptyFields() {
    for(int i = 0; i < this._controllers.length; i++) {
      if (this._controllers.elementAt(i).text == '') {
        return false;
      }
    }
    return true;
  }
}

class NewRoomPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NewRoomPage();
}
