import Vue from 'vue'
import Router from 'vue-router'
import ShareCities from '@/components/ShareCities'
import Room from '@/components/Room'
import Login from '@/components/Login'
import RoomDefinitions from '@/components/RoomDefinitions'
import SignUp from '@/components/SignUp'
import admin from '@/components/admin'
import AboutProject from '@/components/AboutProject'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'ShareCities',
            component: ShareCities
        },

        {
            path: '/locals/:_id',
            name: 'Room',
            component: Room
        },

        {
            path: '/login',
            name: 'Login',
            component: Login
        },

        {
            path: '/local/:_id',
            name: 'RoomDefinitions',
            component: RoomDefinitions
        },

        {
            path: '/signUp',
            name: 'SignUp',
            component: SignUp
        },

        {
            path: '/admin',
            name: 'admin',
            component: admin
        },

        {
            path: '/aboutProject',
            name: 'AboutProject',
            component: AboutProject
        }
    ]
})