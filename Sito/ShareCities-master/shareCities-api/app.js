const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
var cors = require('cors');

const activityRoutes = require('./api/routes/activities');
const localRoutes = require('./api/routes/locals');
const imageRoutes = require('./api/routes/images');

app.set('secretKey', 'nodeRestApi'); // jwt secret token

mongoose.connect(
    "mongodb://localhost:27017/shareCities", {
        useMongoClient: true
    }
);
mongoose.Promise = global.Promise;

//for cors permissions
app.use(cors({origin: '*'}));

app.use(morgan("dev"));
app.use('/images', express.static('images'));
app.use('/profiles', express.static('profiles'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Routes which should handle requests
app.use("/locals", localRoutes);
app.use("/activities", activityRoutes);
app.use("/images", imageRoutes);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
