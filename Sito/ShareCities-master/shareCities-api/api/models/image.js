const mongoose = require('mongoose');

const imageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    local: { type: mongoose.Schema.Types.ObjectId, ref: 'Local' },
    info: { type: String },
    number: { type: String },
});

module.exports = mongoose.model('Image', imageSchema);