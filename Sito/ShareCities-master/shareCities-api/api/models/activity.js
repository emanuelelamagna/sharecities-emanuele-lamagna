const mongoose = require('mongoose');

const activitySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    local: { type: mongoose.Schema.Types.ObjectId, ref: 'Local' },
    nameOfActivity: { type: String, required: true },
    typeOfActivity: { type: String },
    where: { type: String },
    duration: { type: String },
    aboutActivity: { type: String },
    schedule: { type: String },
    info: { type: String }
});

module.exports = mongoose.model('Activity', activitySchema);