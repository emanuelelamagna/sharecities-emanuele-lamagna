const mongoose = require("mongoose");

const Image = require("../models/image");
const Local = require("../models/local");

exports.images_get_all = (req, res, next) => {
    Image.find()
        .select("local _id info number")
        .populate("local", "number")
        .exec()
        .then(docs => {
            res.status(200).json({
                count: docs.length,
                images: docs.map(doc => {
                    return {
                        _id: doc._id,
                        local: doc.local,
                        info: doc.info,
                        number: doc.number
                    };
                })
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.images_create_image = (req, res, next) => {
    Image.find({ local: req.body.localId, number: req.body.number })
        .then(image => {
            if (image.length > 0) {
                return res.status(409).json({
                    message: "Image already exists"
                });
            } else {
                Local.findById(req.body.localId)
                    .then(local => {
                        if (!local) {
                            return res.status(404).json({
                                message: "Local not found"
                            });
                        }
                        const im = new Image({
                            _id: mongoose.Types.ObjectId(),
                            local: req.body.localId,
                            info: req.body.info,
                            number: req.body.number
                        });
                        return im.save();
                    })
                    .then(result => {
                        res.status(201).json({
                            message: "Image stored"
                        });
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
            }
        });
};

exports.images_get_image = (req, res, next) => {
    Image.find({ _id: req.body.localId, number: req.body.number })
        .populate("local")
        .exec()
        .then(image => {
            if (!image) {
                return res.status(404).json({
                    message: "Image not found"
                });
            }
            res.status(200).json({
                image: image,
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.image_no_id = (req, res, next) => {
    console.log("inizio");
    Image.find({ local: req.body.localId, number: req.body.number })
        .populate("local")
        .exec()
        .then(image => {
            console.log(image);
            console.log("dentro");
            if (!image) {
                return res.status(404).json({
                    message: "Image not found"
                });
            }
            res.status(200).json({
                image: image[0]['_doc']['info'],
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.images_delete_image = (req, res, next) => {
    Image.remove({ _id: req.params.imageId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Image deleted",
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.images_update_image = (req, res, next) => {
    let info = req.body.info;
    Image.updateOne({ _id: req.params.imageId}, { $set: { "info": info } })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Image updated"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};