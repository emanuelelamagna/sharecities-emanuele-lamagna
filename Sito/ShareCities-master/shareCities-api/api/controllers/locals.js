const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Local = require("../models/local");
const c = require('../../compCVJimp');
const fs = require('fs');

exports.local_signup = (req, res, next) => {
    console.log(req.body);
    let dst = './images/' + req.body.email + ".png";
    let src = 'room' + req.body.type + '.png';
    console.log("DTS = ", dst);
    console.log("SRC = ", src);

    fs.copyFile(src, dst, (err) => {
        if (err) throw err;
    });
    fs.copyFile('userPicture.png', './profiles/' + req.body.email + ".png", (err) => {
        if (err) throw err;
    });
    Local.find({ email: req.body.email })
        .exec()
        .then(local => {
            if (local.length >= 1) {
                return res.status(409).json({
                    message: "Mail exists"
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {

                        console.log("Response received-- start");
                        console.log(req);
                        console.log("Response received--- end");

                        const local = new Local({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash,
                            name: req.body.name,
                            birthday: req.body.birthday,
                            citizenship: req.body.citizenship,
                            languages: req.body.languages,
                            citiesToShare: req.body.citiesToShare,
                            job: req.body.job,
                            aboutMe: "Hi! I'm a new user!",
                            localImage: req.body.localImage
                        });
                        local
                            .save()
                            .then(result => {
                                console.log(result);
                                res.status(201).json({
                                    message: "Account created successfully!",
                                    id: local._id
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).json({
                                    error: err
                                });
                            });
                    }
                });
            }
        });
};

exports.local_login = (req, res, next) => {
    Local.find({ email: req.body.email })
        .exec()
        .then(local => {
            if (local.length < 1) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            
            bcrypt.compare(req.body.password, local[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: "Auth failed"
                    });
                }
                if (result) {
                    const token = jwt.sign({ email: local[0].email, localId: local[0]._id, name: local[0].name }, req.app.get('secretKey'), { expiresIn: '1h' });
                    
                    return res.status(200).json({
                        message: "Auth successful",
                        token: token,
                        localId: local[0]._id,
                        name: local[0].name
                    });
                    
                }
                res.status(401).json({
                    message: "Auth failed"
                });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.locals_get_all = (req, res, next) => {
    Local.find()
        .select("_id name email birthday citizenship languages citiesToShare job aboutMe interests localImage")
        .exec()
        .then(docs => {
            res.status(200).json(docs);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
        
};

exports.locals_cities = (req, res, next) => {
    console.log("Chiamato get cities");
    res.status(200).json(
        { 
        0 : {
            "id" : "1",
            "name" : "Cesena",
            "country" : "Italy",
            "province" : "Forlì-Cesena",
            "longitude" : 12.2355729,
            "latitude" : 44.1477982,
            },
        },
    );
};

exports.local_delete = (req, res, next) => {
    Local.remove({ _id: req.params.localId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Local deleted"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.locals_get_local = (req, res, next) => {
    const id = req.params.localId;
    Local.findById(id)
        .select("_id name email aboutMe localImage instagram facebook interests phoneNumber")
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if (doc) {
                res.status(200).json({
                    local: doc
                });
            } else {
                res
                    .status(404)
                    .json({ message: "No valid entry found for provided ID" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.locals_get_local_by_mail = (req, res, next) => {
    Local.find({ email : req.body.email })
        .select("id name email interests")
        .exec()
        .then(docs => {
            console.log(docs[0]['_doc']);
            res.status(200).json({
                id: docs[0]['_doc']['_id'],
                name: docs[0]['_doc']['name'],
                email: req.body.email,
                interests: docs[0]['_doc']['interests'],
                ciao: "ciao",
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.locals_update_local = (req, res, next) => {  
    c.mergeImages(req.body.position, req.body.remove, req.body.email, req.body.imageName).then(resMerge => {
        let files = fs.readdirSync('./uploads');
        if(files.includes(req.body.imageName)){
            fs.unlinkSync('./uploads/'+ req.body.imageName);
        }
        res.status(200).json({ stat: "ok"});
    });
};

exports.locals_change_profile_photo = (req, res, next) => {
    c.resizeImages(req.body.name, req.body.email).then(resResize => {
        let files = fs.readdirSync('./uploads');
        if(files.includes(req.body.name)){
            fs.unlinkSync('./uploads/'+ req.body.name);
        }
        res.status(200).json({ stat: "ok"});
    });
    
};

exports.locals_rooms = (req, res, next) => {
    Local.find({ citizenship : req.body.country, citiesToShare : req.body.city })
        .select("_id name email facebook instagram citizenship languages citiesToShare phoneNumberv aboutMe localImage")
        .exec()
        .then(docs => {
            res.status(200).json(docs);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.locals_get_room = (req, res, next) => {
    str = "http://localhost:3000/images/" + req.body.email + ".png";
    res.status(200).json({ 
        name: str
    });
};

exports.locals_update_bio = (req, res, next) => {
    let id = null;
    let biot = null;
    for (const ops of req.body) {
        if(ops.propName=="id"){
            id = ops.value;
        } else if(ops.propName=="aboutMe") {
            biot = ops.value;
        }
    }
    Local.updateOne({ _id: id}, { $set: {"aboutMe": biot} })
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json({
                message: "Local user updated",
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.locals_update_contacts = (req, res, next) => {
    let id = req.body.localId;
    let phone = req.body.phone;
    let face = req.body.facebook;
    let insta = req.body.instagram;
    Local.updateOne({ _id: id}, { $set: {"phoneNumber": phone, "facebook": face, "instagram": insta,} })
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json({
                message: "Local user updated",
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.local_update_interests = (req, res, next) => {
    let id = null;
    let inter = null;
    for (const ops of req.body) {
        if(ops.propName=="id"){
            id = ops.value;
        } else if(ops.propName=="interests") {
            inter = ops.value;
        }
    }
    Local.updateOne({ _id: id}, { $set: {"interests": inter} })
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json({
                message: "Local user updated",
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};
