const express = require("express");
const router = express.Router();
const ImagesController = require('../controllers/images');

// Handle incoming GET requests to /places
router.get("/", ImagesController.images_get_all);

router.post("/", ImagesController.images_create_image);

router.get("/:imageId", ImagesController.images_get_image);

router.post("/imageinfo", ImagesController.image_no_id);

router.patch("/:imageId", ImagesController.images_update_image);

router.delete("/:imageId", ImagesController.images_delete_image);


module.exports = router;