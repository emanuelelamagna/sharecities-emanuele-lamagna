const express = require("express");
const router = express.Router();
const ActivitiesController = require('../controllers/activities');

// Handle incoming GET requests to /places
router.get("/", ActivitiesController.activities_get_all);

router.post("/", ActivitiesController.activities_create_activity);

router.get("/:activityId", ActivitiesController.activities_get_activity);

router.patch("/:activityId", ActivitiesController.activities_update_activity);

router.delete("/:activityId", ActivitiesController.activities_delete_activity);


module.exports = router;