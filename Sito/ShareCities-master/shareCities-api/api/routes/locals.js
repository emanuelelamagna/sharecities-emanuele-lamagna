const express = require("express");
const router = express.Router();
const multer = require('multer');
const LocalController = require('../controllers/locals');
const fs = require('fs');


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    },
    
});

const fileFilter = (req, file, cb) => {
    let files = fs.readdirSync('./images');
    if(files.includes(file.originalname)){
        fs.unlinkSync('./images/'+ file.originalname);
    }
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    /*
    limits: {
        fileSize: 1024 * 1024 * 5
    },*/
    fileFilter: fileFilter
});

router.get("/", LocalController.locals_get_all);

router.get("/cities", LocalController.locals_cities);

router.post("/rooms", LocalController.locals_rooms);

router.post("/room", LocalController.locals_get_room);

router.post("/signup", LocalController.local_signup);

router.post("/interests", LocalController.local_update_interests);

router.post("/login", LocalController.local_login);

router.post("/infos", LocalController.locals_update_bio);

router.post("/contacts", LocalController.locals_update_contacts);

router.post("/localmail", LocalController.locals_get_local_by_mail);

router.get("/:localId", LocalController.locals_get_local);

router.patch("/profile", upload.single('profile'), LocalController.locals_change_profile_photo);

router.patch("/:localId", upload.single("provaImmagine"), LocalController.locals_update_local);

router.delete("/:localId", LocalController.local_delete);

module.exports = router;