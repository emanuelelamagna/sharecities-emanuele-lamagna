const Jimp = require('jimp');
const fs = require('fs');

module.exports = {
    mergeImages: async function (n, rem, name, newImage){   //il primo parametro è il numero del quadro, il secondo se va rimosso o no, il terzo è l'email, usata per salvare la foto
        let back = await Jimp.read('./images/' + name + '.png');
        let dst = cv.matFromImageData(back.bitmap);
        let src = null;
        if(rem==0){
            let m = await Jimp.read('./uploads/' + newImage);
            src = cv.matFromImageData(m.bitmap);
        } else if(rem==1) {
            let m = await Jimp.read('./empty.png');
            src = cv.matFromImageData(m.bitmap);
        }
        
        let M = null;

        if(n<5){
            let dsize = new cv.Size(dst.cols, dst.rows);
            let h = src.rows;
            let w = src.cols;

            const ar = [
                [1162, 1754, 1414, 1754, 1162, 2144, 1414, 2144], //primo sotto OLA
                [1611, 1473, 1832, 1504, 1611, 2118, 1832, 2110], //secondo, il più lungo
                [1987, 1520, 2148, 1561, 1987, 1826, 2148, 1842], //terzo, piccolo sopra HELLO
                [7288, 1452, 7522, 1465, 7288, 1791, 7522, 1791]  //quarto quadro, in fondo
            ];
            
            let m_pts = null;
            m_pts = cv.matFromArray(4, 1, cv.CV_32FC2, [0, 0, w-1, 0, 0, h-1, w-1, h-1]);
            
            let pts = cv.matFromArray(4, 1, cv.CV_32FC2, ar[n-1]);
            M = cv.getPerspectiveTransform(m_pts, pts);

            cv.warpPerspective(src, dst, M, dsize, cv.INTER_LINEAR, cv.BORDER_TRANSPARENT, new cv.Scalar());
        } else {
            let rect = new cv.Rect(3176, 1409, 652, 434);
            let roi = dst.roi(rect);
            let mt = await Jimp.read('./roi.png');
            let mask = cv.matFromImageData(mt.bitmap);

            let maskSize = new cv.Size(mask.cols, mask.rows);

            cv.bitwise_and(roi, mask, roi);
            cv.threshold(mask, mask, 127, 255, cv.THRESH_BINARY_INV);
            cv.resize(src, src, maskSize);
            cv.bitwise_and(src, mask, src);
            cv.bitwise_or(roi, src, roi);
        }
        
        new Jimp({
            width: dst.cols,
            height: dst.rows,
            data: Buffer.from(dst.data)
        }).write('./images/' + name + '.png');
        dst.delete(); 
        if(n<5){
            src.delete();
            M.delete();
        }
    },
    resizeImages: async function (name, email){
        let jimpImage = await Jimp.read('./uploads/' + name);
        let src = cv.matFromImageData(jimpImage.bitmap);
        let dst = new cv.Mat();

        cv.resize(src, dst, new cv.Size(500, 500));

        new Jimp({
            width: dst.cols,
            height: dst.rows,
            data: Buffer.from(dst.data)
        }).write('./profiles/' + email + '.png');
        src.delete();
        dst.delete();
    }
}

cv = require('./opencv.js');